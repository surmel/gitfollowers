<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp;
use GuzzleHttp\Exception\GuzzleException as Exception;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    
    protected function validator($request)
    {
        return Validator::make($request, [
            'username' => 'required',            
        ]);
    }

    public function index()
    {
        return view('index');
    }

    public function search(Request $request)
    {        
        $data = $request->only(['username']);
    	$error = $this->validator($data)->errors();
    	$response['errors']=$error->messages();
        if(!empty($error->messages())){
            return response()->json($response, 401);
        }        

        $client = new GuzzleHttp\Client(['verify' => false]);          
            try {                
                $responseBody = $client->get('https://api.github.com/users/'.$data['username']);                    
            } catch (Exception $exception) {                
                if($exception){                    
                    echo json_encode(response()->json('no_data'));
                    exit;
                }
            }            
            $userData = json_decode($responseBody->getBody());
            $responseData['userData'] = $userData;
            try {                
                $resBody = $client->get('https://api.github.com/users/'.$data['username'].'/followers');                    
            } catch (Exception $exception) {                
                if($resBody){
                    $responseData['followers'] = [];
                }
            }
            $followers = json_decode($resBody->getBody());
            $responseData['followers'] = $followers;
            echo json_encode($responseData);
            exit;            
    }
}
