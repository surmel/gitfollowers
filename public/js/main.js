$(document).ready(function(){  
    $(document).keypress(function(e) {
        if(e.which == 13) {
             $('.find').click();
        }
    });
    $('.find').click(function(){        
        var username = $('#username').val();                   
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.find').attr('disabled',true);
        $.ajax({
            type:'post',
            url:'/search',
            dataType:'json',
            data:{username:username},
            success: function(res){                
                if(res.original === 'no_data'){
                    $('.find').attr('disabled',false);
                    $('.userNotFound').fadeIn('fast');
                    setTimeout(function(){
                        $('.userNotFound').fadeOut('fast');
                    },3000);
                }                            
                else {
                    $('.search_part').css('display','none');                    
                    $('#places').empty();
                    $('.userImage').attr('src', res['userData'].avatar_url);
                    $('.name').text(res['userData'].name);
                    $('.followersCount').text('Followers: '+res['userData'].followers);                        
                    $('.userLink').attr('href', res['userData'].html_url);                                                                      
                    $('.userId').val(res['userData'].login);                                                                      
                    $.each(res['followers'], function(key, value){                        
                        $('.followerList').append('<div class="col-md-2 followerImage">\n\
                                                    <a href="'+value['html_url']+'">\n\
                                                        <img src="'+value['avatar_url']+'" class="img-thumbnail">\n\
                                                    </a>\n\
                                                </div>'
                        );
                    });                    
                    if(res['userData'].followers > 30){
                        $('.info_part').append('<input type="button" value="Load More" class="btn btn-primary loadMore">');
                    }
                }                
                $('.info_part').fadeIn('fast');
                
            },
            error:function(err){
                $('.find').attr('disabled',false);
                if(err.responseJSON){
                    var errors = err.responseJSON.errors;                                        
                    if(errors.username){
                        $('#user_name').html(errors.username[0]);
                        $('#username').parent('div').addClass('has-error');
                    }
                }
            }
        });
    });
    $('.back_button').click(function(){   
        $('.find').attr('disabled',false);
        $('#username').val('');        
        $('.info_part').css('display','none');
        $('.search_part').fadeIn('fast');
        $('.loadMore').remove();
        $('.followerList').empty();
    });
    $('#username').on('keyup',function(){
        if($(this).parent('div').hasClass('has-error')){
            $(this).parent('div').removeClass('has-error');
            $('#user_name').html('');
        }
    });   
    $('.info_part').delegate('.loadMore', 'click', function(){        
        var username = $('.userId').val();
        var page = $('.page').val();
        $('.page').val(parseInt(page)+1);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var moreFollowers = JSON.parse(this.responseText);
                $.each(moreFollowers, function(key, value){                        
                    $('.followerList').append('<div class="col-md-2 followerImage">\n\
                                                <a href="'+value['html_url']+'">\n\
                                                    <img src="'+value['avatar_url']+'" class="img-thumbnail">\n\
                                                </a>\n\
                                            </div>'
                    );
                });                    
                if(moreFollowers.length < 30){
                    $('.loadMore').css('display','none');
                }
            }
        };
        xmlHttp.open( "GET", 'https://api.github.com/users/'+username+'/followers?page='+page, true);
        xmlHttp.send();                
    });
});

