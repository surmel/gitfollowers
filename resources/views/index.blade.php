<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Title -->
        <title>{{ config('app.name') }}</title>
        <link rel="shortcut icon" href="">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">        
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="background_image"></div>
        <header>
            <p class="title">GitHub Followers Task</p>            
        </header>
        <div class="container">              
            <div class="row">                
                <div class="col-lg-3 col-centered search_part">
                    <div class="alert alert-danger userNotFound">
                        No users found
                    </div>
                    <div class="form-group">                        
                        <input type="text" class="form-control" id="username" placeholder="Enter Username">
                        <span class="input_error" id="user_name"></span>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success col-sm-12 find">Search</button>                                                
                    </div>
                    
                </div>   
                <div class="col-md-8 col-centered info_part">
                    <input type="button" value="Back to search" class="btn btn-warning back_button">  
                    <div class="row">
                        <div class="col-md-4">
                            <img src="" class="img-thumbnail userImage">
                        </div>
                        <div class="col-md-8">
                            <p class="infoText name"></p>
                            <p class="infoText followersCount"></p>
                            <a href="" class="infoText userLink" target="_blank">Go to GitHub page</a>
                            <input type="hidden" class="userId" value="">
                            <input type="hidden" class="page" value="2">
                        </div>
                    </div>
                    <div class="row followerList">
                        
                    </div>
                    
                    
                </div>                
            </div>
        </div>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>        
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </body>
</html>
